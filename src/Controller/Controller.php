<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class Controller extends AbstractController
{
    /**
     * Save Entity to Database
     *
     * @param Entity $entity
     * @return void
     */
    protected function save($entity)
    {
        $entity_manager = $this->getDoctrine()->getManager();
        $entity_manager->persist($entity);
        $entity_manager->flush();
    }
}
