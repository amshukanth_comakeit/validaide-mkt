<?php

namespace App\Controller;

use App\Entity\Mkt;
use App\Form\MktFormType;
use App\Repository\MktRepository;
use App\Services\CSVParserService;
use App\Services\DatasetProcessor;
use App\Services\FileUploadService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MktController extends Controller
{
    private const ACTIVATION_ENERGY = 83.144;

    /**
     * @Route("/", name="mkt")
     */
    public function index(
        Request $request,
        FileUploadService $file_upload_service,
        CSVParserService $csv_parser_service,
        DatasetProcessor $dataset_processor,
        MktRepository $mkt_repository
    ): Response {

        $mkt = new Mkt();
        $mkt_form = $this->createForm(MktFormType::class, $mkt);
        $mkt_form->handleRequest($request);
        if (
            $mkt_form->isSubmitted()
            && $mkt_form->isValid()
            && $dataset_file = $mkt_form->get('dataset')->getData()
        ) {
            $datasets_dir = $this->getParameter('datasets_dir');
            if (!$new_file_name = $file_upload_service->upload($dataset_file, $datasets_dir)) {
                return $this->render('error.html.twig', [ 'error' => 'Upload Error' ]);
            }
            if (!empty($dataset = $csv_parser_service->parseCSV($datasets_dir, $new_file_name))) {    // parse CSV File
                $mkt->setActivationEnergy(self::ACTIVATION_ENERGY);
                $mkt->setIpAddress($request->getClientIp());
                $mkt->setDataset($new_file_name);
                $mkt_result = $dataset_processor->calculateMKT($dataset);   // calculate Mean Kinetic Energy
                $mkt->setResult($mkt_result);
                $this->save($mkt);
            } else {
                $error = 'Invalid File';
            }
        }

        // TODO:: Need to improve on doctrine
        $mkt_records = $mkt_repository->createQueryBuilder('mkt')
                                    ->orderBy('mkt.id', 'DESC')
                                    ->getQuery()
                                    ->getResult();

        return $this->render('mkt/index.html.twig', [
            'mkt_form'    => $mkt_form->createView(),
            'result'      => $mkt_result ?? '',
            'mkt_records' => $mkt_records,
            'error'       => $error ?? ''
        ]);
    }
}
