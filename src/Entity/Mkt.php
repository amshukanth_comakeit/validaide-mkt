<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MktRepository::class)
 */
class Mkt
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $dataset;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $activation_energy;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $ip_address;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $result;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    public function __construct()
    {
        $this->created_at = new \DateTime();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDataset(): ?string
    {
        return $this->dataset;
    }

    public function setDataset(string $dataset): self
    {
        $this->dataset = $dataset;

        return $this;
    }

    public function getActivationEnergy(): ?string
    {
        return $this->activation_energy;
    }

    public function setActivationEnergy(string $activation_energy): self
    {
        $this->activation_energy = $activation_energy;

        return $this;
    }

    public function getIpAddress(): ?string
    {
        return $this->ip_address;
    }

    public function setIpAddress(string $ip_address): self
    {
        $this->ip_address = $ip_address;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function getResult()
    {
        return $this->result;
    }

    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }
}
