<?php

namespace App\Services;

use Symfony\Component\Finder\Finder;

class CSVParserService
{

    /**
     * Parse a csv file
     *
     * @param string $directory
     * @param string $filename
     * @return array
     */
    public function parseCSV($directory, $filename)
    {
        $finder = new Finder();
        $finder->files()
            ->in($directory)
            ->name($filename);

        foreach ($finder as $file) {
            $csv = $file;
        }

        $rows = array();
        if (($handle = fopen($csv->getRealPath(), "r")) !== false) {
            $i = 0;
            while (($data = fgetcsv($handle, null, ",")) !== false) {
                $i++;
                if ($i == 1) {
                    continue;
                }
                foreach ($data as $item) {
                    if (!is_numeric($item)) {
                        return [];
                    }
                }
                $rows[] = $data;
            }
            fclose($handle);
        }

        return $rows;
    }
}
