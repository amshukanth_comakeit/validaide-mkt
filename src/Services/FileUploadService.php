<?php

namespace App\Services;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\String\Slugger\SluggerInterface;

class FileUploadService
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SluggerInterface
     */
    private $slugger;

    public function __construct(SluggerInterface $slugger, LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->slugger = $slugger;
    }

    /**
     * Copy Uploaded file to destination path
     *
     * @param UploadedFile $file
     * @param string $destination_path
     * @return string|bool
     */
    public function upload($file, $destination_path)
    {
        $original_filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $slugged_filename = $this->slugger->slug($original_filename);
        $new_filename = $slugged_filename . '-' . uniqid() . '.' . $file->guessExtension();
        try {
            if (is_dir($destination_path) || (mkdir($destination_path, 0777, true) && is_dir($destination_path)) && is_writable($destination_path)) {
                $file->move(
                    $destination_path,
                    $new_filename
                );
                return $new_filename;
            }
        } catch (FileException $e) {
            $this->logger->error("Couldn't upload File : " . $e->getMessage());
            return false;
        }
    }
}
