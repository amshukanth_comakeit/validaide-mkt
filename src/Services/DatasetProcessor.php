<?php

namespace App\Services;

class DatasetProcessor
{
    private const GAS_CONSTANT = 8.314;
    private const ACTIVATION_ENERGY = 83.144;
    private const KELVIN = 273.15; // Kelvin


    /**
     * Calculate Mean Kinetic Temperature
     *
     * @param array $dataset
     * @return float
     */
    public function calculateMKT($dataset)
    {
        // Ref: https://en.wikipedia.org/wiki/Mean_kinetic_temperature
        $activating_gas = self::ACTIVATION_ENERGY / self::GAS_CONSTANT;
        $time_intervals_sum = 0;
        $log_numerator_sum = 0;
        foreach ($dataset as $data) {
            $time = $data[0];
            $temperature = $data[1] + self::KELVIN; // Celsius to Kelvin
            $activating_gas_per_temperature = $activating_gas / $temperature;
            $time_intervals_sum += $time;
            $log_numerator_sum += ($time * exp(-$activating_gas_per_temperature));
        }

        return round($activating_gas / (log($time_intervals_sum) - log($log_numerator_sum)), 3) - self::KELVIN;
    }
}
