<?php

namespace App\Tests;

use App\Services\FileUploadService;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TestCase extends WebTestCase
{
    protected $fileupload;
    protected $datasets_dir;
    protected $client;
    
    public function setUp(): void
    {
        // self::bootkernel();
        $this->client = static::createClient();
        $container = self::$container;
        $this->fileupload = $container->get(FileUploadService::class);
        $this->datasets_dir = $container->getParameter('datasets_dir');
    }
}
