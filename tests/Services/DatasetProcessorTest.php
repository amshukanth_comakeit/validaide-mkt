<?php

namespace App\Tests\Services;

use App\Services\CSVParserService;
use App\Services\DatasetProcessor;
use App\Services\FileUploadService;
use App\Tests\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class DatasetProcessorTest extends TestCase
{
    public function testCalculateMKT()
    {
        $header = 'Time,Temperature';
        $row1 = '10,20';
        $row2 = '20,30';
        $content = implode("\n", [$header, $row1, $row2]);
        file_put_contents($this->datasets_dir . '/test.csv', $content);
        $dataset = (new CSVParserService())->parseCSV($this->datasets_dir, 'test.csv');
        $this->assertCount(2, $dataset);
        $calculate_mkt = (new DatasetProcessor())->calculateMKT($dataset);
        $this->assertIsNumeric($calculate_mkt);
    }
}