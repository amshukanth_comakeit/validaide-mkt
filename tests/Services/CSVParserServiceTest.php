<?php

namespace App\Tests\Services;

use App\Services\CSVParserService;
use App\Services\FileUploadService;
use App\Tests\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class CSVParserServiceTest extends TestCase
{
    public function testCSVParse()
    {
        $header = 'Time,Temperature';
        $row1 = '10,20';
        $row2 = '20,30';

        $content = implode("\n", [$header, $row1, $row2]);
        file_put_contents($this->datasets_dir . '/test.csv', $content);
        $rows = (new CSVParserService())->parseCSV($this->datasets_dir, 'test.csv');
        $this->assertCount(2, $rows);

        $row3 = '30,NaN,34';
        $content .= implode("\n", [$content, $row3]);
        file_put_contents($this->datasets_dir . '/test.csv', $content);
        $rows = (new CSVParserService())->parseCSV($this->datasets_dir, 'test.csv');
        $this->assertCount(0, $rows);

    }
}