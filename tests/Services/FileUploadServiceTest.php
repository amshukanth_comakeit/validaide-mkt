<?php

namespace App\Tests\Services;

use App\Services\FileUploadService;
use App\Tests\TestCase;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploadServiceTest extends TestCase
{
    public function testFileUpload(): void
    {
        $header = 'Time,Temperature';
        $row1 = '10,20';
        $row2 = '20,30';

        $content = implode("\n", [$header, $row1, $row2]);
        file_put_contents($this->datasets_dir . '/test.csv', $content);

        $file = new UploadedFile($this->datasets_dir .'/test.csv', 'test.csv', null, null, true);
        $new_file = $this->fileupload->upload($file, $this->datasets_dir);
        $this->assertFileExists($this->datasets_dir . '/' .$new_file);
    }
}
