<?php

namespace App\Tests\Controller;

use App\Form\MktFormType;
use App\Services\CSVParserService;
use App\Services\FileUploadService;
use App\Tests\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class MktControllerTest extends TestCase
{
    public function testGetMktRecords()
    {
        $this->client->request('GET', '/');
        $this->assertResponseIsSuccessful();
    }

    public function testPostMkt()
    {
        $header = 'Time,Temperature';
        $row1 = '10,20';
        $row2 = '20,30';

        $content = implode("\n", [$header, $row1, $row2]);
        file_put_contents($this->datasets_dir . '/test.csv', $content);
        $file = new UploadedFile($this->datasets_dir .'/test.csv', 'test.csv', null, null);

        // Valid File & Response
        $crawler = $this->client->request('GET', '/');
        $this->client->followRedirects();
        $form = $crawler->selectButton('Calculate')->form();
        $form['mkt_form[dataset]'] = $file;
        $this->client->submit($form);
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('body > div.container-fluid.mt-2 > div:nth-child(3) > div > span', 'Mean Kinetic Temperature');

        // Invalid File
        $row3 = '343,sdfsd';
        $content = implode("\n", [$content, $row3]);
        file_put_contents($this->datasets_dir . '/test.csv', $content);
        $file = new UploadedFile($this->datasets_dir .'/test.csv', 'test.csv', null, null);
        $form['mkt_form[dataset]'] = $file;
        $this->client->submit($form);

        $this->assertSelectorTextSame('body > div.container-fluid.mt-2 > div:nth-child(3) > div > span', 'Invalid File');
    }
}
